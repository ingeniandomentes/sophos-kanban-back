/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.dto;

import javax.validation.constraints.NotBlank;

/**
 *
 * @author Ricardo
 */
public class TaskDto {
    
    @NotBlank
    private String name;
    @NotBlank
    private String description;
    @NotBlank
    private Integer userId;
    @NotBlank
    private String username;
    @NotBlank
    private int status;

    public TaskDto() {
    }

    public TaskDto(String name, String description, Integer userId,String username, int status) {
        this.name = name;
        this.description = description;
        this.userId = userId;        
        this.username = username;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    
}
