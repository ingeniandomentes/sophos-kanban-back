/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.controller;

import com.sophos.back.dto.Message;
import com.sophos.back.dto.TaskDto;
import com.sophos.back.entity.Task;
import com.sophos.back.service.TaskService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ricardo
 */
@RestController
@RequestMapping("/task")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class TaskController {
    
    @Autowired
    TaskService taskService;
    
    @ApiOperation("Method for get all tasks")
    @GetMapping("/all")
    public ResponseEntity<List<Task>> list(){
        List<Task> list = taskService.list();
        return new ResponseEntity<List<Task>>(list, HttpStatus.OK);
    }
    
    @ApiOperation("Method for get task by id")
    @GetMapping("/byTaskId/{id}")
    public ResponseEntity<Task> getById(@PathVariable("id") int id){
        if(!taskService.existsById(id))
            return new ResponseEntity(new Message("Task does not exist"), HttpStatus.NOT_FOUND);
        Task task = taskService.getById(id).get();
        return new ResponseEntity(task, HttpStatus.OK);
    }
    
    @ApiOperation("Method for get task by user id")
    @GetMapping("/byUserId/{id}")
    public ResponseEntity<Task> getByUserId(@PathVariable("id") int id){
        if(!taskService.existsByUserId(id))
            return new ResponseEntity(new Message("User id does not have any task"), HttpStatus.NOT_FOUND);
        Task task = taskService.getByUserId(id).get();
        return new ResponseEntity(task, HttpStatus.OK);
    }
    
    @ApiOperation("Method for get task by username")
    @GetMapping("/byUsername/{username}")
    public ResponseEntity<List<Task>> getByUsername(@PathVariable("username") String username){
        if(!taskService.existsByUsername(username))
            return new ResponseEntity(new Message("Username does not have any task"), HttpStatus.NOT_FOUND);
        List<Task> task = taskService.getByUsername(username);
        return new ResponseEntity(task, HttpStatus.OK);
    }
    
    @ApiOperation("Method for create new task")
    @PostMapping("/create")    
    public ResponseEntity<?> create(@RequestBody TaskDto taskDto){
        if(StringUtils.isBlank(taskDto.getName()))
            return new ResponseEntity(new Message("Name is required"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(taskDto.getDescription()))
            return new ResponseEntity(new Message("Description is required"), HttpStatus.BAD_REQUEST);
        if(taskDto.getUserId() < 0)
            return new ResponseEntity(new Message("User id is required"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(taskDto.getUsername()))
            return new ResponseEntity(new Message("Username is required"), HttpStatus.BAD_REQUEST);
        if(taskDto.getStatus() < 0)
            return new ResponseEntity(new Message("Status is required"), HttpStatus.BAD_REQUEST);
        Task task = new Task(taskDto.getName(), taskDto.getDescription(), taskDto.getUserId(),taskDto.getUsername(),taskDto.getStatus());
        taskService.save(task);
        return new ResponseEntity(new Message("Task created successfuly"), HttpStatus.OK);
    }
    
    @ApiOperation("Method for update task")
    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id") int id,@RequestBody TaskDto taskDto){
        if(!taskService.existsById(id))
            return new ResponseEntity(new Message("Task does not exist"), HttpStatus.NOT_FOUND);
        if(taskService.existsByName(taskDto.getName()) && taskService.getByName(taskDto.getName()).get().getId()!= id)
            return new ResponseEntity(new Message("Task name has already taken"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(taskDto.getName()))
            return new ResponseEntity(new Message("Name is required"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(taskDto.getDescription()))
            return new ResponseEntity(new Message("Description is required"), HttpStatus.BAD_REQUEST);
        if(taskDto.getUserId() < 0)
            return new ResponseEntity(new Message("User id is required"), HttpStatus.BAD_REQUEST);
        if(taskDto.getStatus() < 0)
            return new ResponseEntity(new Message("Status is required"), HttpStatus.BAD_REQUEST);
        Task task = taskService.getById(id).get();
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setUserId(taskDto.getUserId());
        task.setStatus(taskDto.getStatus());
        taskService.save(task);
        return new ResponseEntity(new Message("Task updated successfuly"), HttpStatus.OK);
    }
    
    @ApiOperation("Method for delete task")
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") int id){
        if(!taskService.existsById(id))
            return new ResponseEntity(new Message("Task does not exist"), HttpStatus.NOT_FOUND);
        taskService.delete(id);
        return new ResponseEntity(new Message("Task deleted successfuly"), HttpStatus.OK);
    }
}
