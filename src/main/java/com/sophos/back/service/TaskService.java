/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.service;

import com.sophos.back.entity.Task;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sophos.back.repository.TaskRepository;

/**
 *
 * @author Ricardo
 */
@Service
@Transactional
public class TaskService {
    @Autowired
    TaskRepository taskRepository;
    
    public List<Task> list(){
        return taskRepository.findAll();
    }
    
    public Optional<Task> getById(int id){
        return taskRepository.findById(id);
    }
    
    public boolean existsById(int id){
        return taskRepository.existsById(id);
    }
    
    public Optional<Task> getByName(String name){
        return taskRepository.findByName(name);
    }
     
    public boolean existsByName(String name){
        return taskRepository.existsByName(name);
    }
    
    public Optional<Task> getByUserId(int id){
        return taskRepository.findByUserId(id);
    }
     
    public boolean existsByUserId(int id){
        return taskRepository.existsByUserId(id);
    }
    
    public List<Task> getByUsername(String username){
        return taskRepository.findAllByUsername(username);
    }
     
    public boolean existsByUsername(String username){
        return taskRepository.existsByUsername(username);
    }
    
    public Optional<Task> getByStatus(int id){
        return taskRepository.findByStatus(id);
    }
    
    public boolean existsByStatus(int status){
        return taskRepository.existsByStatus(status);
    }
    
    public void save(Task task){
        taskRepository.save(task);
    }
    
    public void delete(int id){
        taskRepository.deleteById(id);
    }
}
