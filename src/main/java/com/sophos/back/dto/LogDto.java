/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.dto;

import java.sql.Timestamp;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author Ricardo
 */
public class LogDto {
    private int id;
    @NotBlank
    private String nameAction;
    @NotBlank
    private String tableAction;
    @NotBlank
    private java.sql.Timestamp dateAction;
    @NotBlank
    private int userIdAction;

    public LogDto() {
    }

    public LogDto(int id, String nameAction, String tableAction, int userIdAction) {
        this.id = id;
        this.nameAction = nameAction;
        this.tableAction = tableAction;
        this.userIdAction = userIdAction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameAction() {
        return nameAction;
    }

    public void setNameAction(String nameAction) {
        this.nameAction = nameAction;
    }

    public String getTableAction() {
        return tableAction;
    }

    public void setTableAction(String tableAction) {
        this.tableAction = tableAction;
    }

    public Timestamp getDateAction() {
        return dateAction;
    }

    public void setDateAction(Timestamp dateAction) {
        this.dateAction = dateAction;
    }

    public int getUserIdAction() {
        return userIdAction;
    }

    public void setUserIdAction(int userIdAction) {
        this.userIdAction = userIdAction;
    }
    
    
}
