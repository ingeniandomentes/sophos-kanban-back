/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.security.enums;

/**
 *
 * @author Ricardo
 */
public enum RolName {
    ROLE_ADMIN, ROLE_USER
}
