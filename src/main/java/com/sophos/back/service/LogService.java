/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.service;

import com.sophos.back.entity.Log;
import com.sophos.back.repository.LogRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ricardo
 */
@Service
@Transactional
public class LogService {
    @Autowired
    LogRepository logRepository;
    
    public List<Log> list(){
        return logRepository.findAll();
    }
    
    public Optional<Log> getById(int id){
        return logRepository.findById(id);
    }
    
    public void save(Log log){
        logRepository.save(log);
    }
    
    public boolean existsById(int id){
        return logRepository.existsById(id);
    }
}
