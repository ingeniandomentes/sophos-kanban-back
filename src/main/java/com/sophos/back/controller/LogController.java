/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.controller;

import com.sophos.back.dto.LogDto;
import com.sophos.back.dto.Message;
import com.sophos.back.entity.Log;
import com.sophos.back.service.LogService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ricardo
 */
@RestController
@RequestMapping("/log")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class LogController {
    @Autowired
    LogService logService;
    
    @ApiOperation("Method for get all logs")
    @GetMapping("/all")
    public ResponseEntity<List<Log>> list(){
        List<Log> list = logService.list();
        return new ResponseEntity<List<Log>>(list, HttpStatus.OK);
    }
    
    @ApiOperation("Method for get logs by id")
    @GetMapping("/byStatusId/{id}")
    public ResponseEntity<Log> getById(@PathVariable("id") int id){
        if(!logService.existsById(id))
            return new ResponseEntity(new Message("Log does not exist"), HttpStatus.NOT_FOUND);
        Log log = logService.getById(id).get();
        return new ResponseEntity(log, HttpStatus.OK);
    }
    
    @ApiOperation("Method for create logs")
    @PostMapping("/create")    
    public ResponseEntity<?> create(@RequestBody LogDto logDto){
        if(StringUtils.isBlank(logDto.getNameAction()))
            return new ResponseEntity(new Message("Name is required"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(logDto.getTableAction()))
            return new ResponseEntity(new Message("Name is required"), HttpStatus.BAD_REQUEST);
        if(logDto.getUserIdAction() < 0)
            return new ResponseEntity(new Message("User id is required"), HttpStatus.BAD_REQUEST);
        Log log = new Log(logDto.getNameAction(), logDto.getTableAction(),logDto.getUserIdAction());
        logService.save(log);
        return new ResponseEntity(new Message("Log created successfuly"), HttpStatus.OK);
    }
}
