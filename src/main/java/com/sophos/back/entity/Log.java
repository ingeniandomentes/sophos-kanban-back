/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.entity;

import com.sophos.back.enums.StatusName;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Ricardo
 */
@Entity(name = "logs")
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    private String nameAction;
    @NotNull
    private String tableAction;
    @NotNull
    private java.sql.Timestamp dateAction;
    @NotNull
    private int userIdAction;

    public Log() {
    }

    public Log(String nameAction, String tableAction, int userIdAction) {
        this.nameAction = nameAction;
        this.tableAction = tableAction;
        this.userIdAction = userIdAction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameAction() {
        return nameAction;
    }

    public void setNameAction(String nameAction) {
        this.nameAction = nameAction;
    }

    public String getTableAction() {
        return tableAction;
    }

    public void setTableAction(String tableAction) {
        this.tableAction = tableAction;
    }

    public Timestamp getDateAction() {
        return dateAction;
    }

    public void setDateAction(Timestamp dateAction) {
        this.dateAction = dateAction;
    }

    public int getUserIdAction() {
        return userIdAction;
    }

    public void setUserIdAction(int userIdAction) {
        this.userIdAction = userIdAction;
    }
    
    
}
