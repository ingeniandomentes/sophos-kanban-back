/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.dto;

import com.sophos.back.enums.StatusName;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author Ricardo
 */
public class StatusDto {
    @NotBlank
    private int id;
    @NotBlank
    private StatusName name;

    public StatusDto() {
    }

    public StatusDto(int id, StatusName name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StatusName getName() {
        return name;
    }

    public void setName(StatusName name) {
        this.name = name;
    }
    
    
}
