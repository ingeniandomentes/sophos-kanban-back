/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.repository;

import com.sophos.back.entity.Task;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ricardo
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Integer>{
    Optional<Task> findByUserId(int id);
    boolean existsByUserId(int id);
    
    Optional<Task> findByName(String name);
    boolean existsByName(String name);
    
    List<Task> findAllByUsername(String username);
    boolean existsByUsername(String username);
    
    Optional<Task> findByStatus(int id);
    boolean existsByStatus(int id);
}
