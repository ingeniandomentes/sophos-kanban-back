/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.service;

import com.sophos.back.entity.Status;
import com.sophos.back.repository.StatusRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ricardo
 */
@Service
@Transactional
public class StatusService {
    
    @Autowired
    StatusRepository statusRepository;
    
    public List<Status> list(){
        return statusRepository.findAll();
    }
    
    public Optional<Status> getById(int id){
        return statusRepository.findById(id);
    }
    
    public Optional<Status> getByName(String name){
        return statusRepository.findByName(name);
    }
    
    public void save(Status status){
        statusRepository.save(status);
    }
    
    public void delete(int id){
        statusRepository.deleteById(id);
    }
    
    public boolean existsById(int id){
        return statusRepository.existsById(id);
    }
    
    public boolean existsByName(String name){
        return statusRepository.existsByName(name);
    }
}
