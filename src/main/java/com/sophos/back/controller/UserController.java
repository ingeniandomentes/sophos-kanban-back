/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.controller;

import com.sophos.back.dto.Message;
import com.sophos.back.dto.TaskDto;
import com.sophos.back.entity.Task;
import com.sophos.back.security.dto.NewUserDto;
import com.sophos.back.security.entity.Rol;
import com.sophos.back.security.entity.User;
import com.sophos.back.security.enums.RolName;
import com.sophos.back.security.service.RolService;
import com.sophos.back.security.service.UserService;
import io.swagger.annotations.ApiOperation;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ricardo
 */
@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class UserController {
    
    @Autowired
    PasswordEncoder passwordEncoder;
    
    @Autowired
    UserService userService;
    
    @Autowired
    RolService rolService;
    
    
    @ApiOperation("Method for get all users")
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/all")
    public ResponseEntity<List<User>> list(){
        List<User> list = userService.list();
        return new ResponseEntity<List<User>>(list, HttpStatus.OK);
    }
    
    @ApiOperation("Method for get user by id")
    @GetMapping("/byUserId/{id}")
    public ResponseEntity<User> getByUserId(@PathVariable("id") int id){
        if(!userService.existsById(id))
            return new ResponseEntity(new Message("User does not exist"), HttpStatus.NOT_FOUND);
        User user = userService.getById(id).get();
        return new ResponseEntity(user, HttpStatus.OK);
    }
    
    @ApiOperation("Method for get user by username")
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/byUsername/{username}")
    public ResponseEntity<User> getByUserId(@PathVariable("username") String username){
        if(!userService.existsByUsername(username))
            return new ResponseEntity(new Message("Username does not exist"), HttpStatus.NOT_FOUND);
        User user = userService.getByUsername(username).get();
        return new ResponseEntity(user, HttpStatus.OK);
    }
    
    @ApiOperation("Method for create new user")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/new")
    public ResponseEntity <?> newUser(@Valid @RequestBody NewUserDto newUserDto, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity(new Message("Bad inputs or invalid email"),HttpStatus.BAD_REQUEST);
        if(userService.existsByUsername(newUserDto.getUsername()))
            return new ResponseEntity(new Message("User has already taken"), HttpStatus.BAD_REQUEST);
        if(userService.existsByEmail(newUserDto.getEmail()))
            return new ResponseEntity(new Message("Email has already taken"), HttpStatus.BAD_REQUEST);
        User user = new User(newUserDto.getName(), newUserDto.getUsername(), newUserDto.getEmail(), passwordEncoder.encode(newUserDto.getPassword()));
        Set<Rol> roles = new HashSet<>();
        roles.add(rolService.getByRolName(RolName.ROLE_USER).get());
        if(newUserDto.getRoles().contains("admin"))
            roles.add(rolService.getByRolName(RolName.ROLE_ADMIN).get());
        user.setRoles(roles);
        userService.save(user);
        return new ResponseEntity(new Message("User created successfully"), HttpStatus.OK);
    }
    
    @ApiOperation("Method for update user")
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateUser(@PathVariable("id") int id,@RequestBody NewUserDto newUserDto){
        if(!userService.existsById(id))
            return new ResponseEntity(new Message("User does not exist"), HttpStatus.NOT_FOUND);
        if(userService.existsByUsername(newUserDto.getUsername()) && userService.getByUsername(newUserDto.getUsername()).get().getId()!= id)
            return new ResponseEntity(new Message("Username has already taken"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(newUserDto.getUsername()))
            return new ResponseEntity(new Message("Username is required"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(newUserDto.getEmail()))
            return new ResponseEntity(new Message("Email is required"), HttpStatus.BAD_REQUEST);
        User user = userService.getById(id).get();
        Set<Rol> roles = new HashSet<>();
        roles.add(rolService.getByRolName(RolName.ROLE_USER).get());
        if(newUserDto.getRoles().contains("admin"))
            roles.add(rolService.getByRolName(RolName.ROLE_ADMIN).get());
        user.setName(newUserDto.getName());
        user.setUsername(newUserDto.getUsername());
        user.setEmail(newUserDto.getEmail());
        user.setPassword(user.getPassword());
        user.setRoles(roles);
        userService.save(user);
        return new ResponseEntity(new Message("User updated successfully"), HttpStatus.OK);
    }
    
    @ApiOperation("Method for delete user")
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") int id){
        if(!userService.existsById(id))
            return new ResponseEntity(new Message("User does not exist"), HttpStatus.NOT_FOUND);
        userService.delete(id);
        return new ResponseEntity(new Message("User deleted successfuly"), HttpStatus.OK);
    }
}
