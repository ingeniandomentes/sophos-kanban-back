/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.controller;

import com.sophos.back.dto.Message;
import com.sophos.back.dto.StatusDto;
import com.sophos.back.entity.Status;
import com.sophos.back.service.StatusService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ricardo
 */
@RestController
@RequestMapping("/status")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class StatusController {
    @Autowired
    StatusService statusService;
    
    @ApiOperation("Method for get all status")
    @GetMapping("/all")
    public ResponseEntity<List<Status>> list(){
        List<Status> list = statusService.list();
        return new ResponseEntity<List<Status>>(list, HttpStatus.OK);
    }
    
    @ApiOperation("Method for get status by id")
    @GetMapping("/byStatusId/{id}")
    public ResponseEntity<Status> getById(@PathVariable("id") int id){
        if(!statusService.existsById(id))
            return new ResponseEntity(new Message("Status does not exist"), HttpStatus.NOT_FOUND);
        Status status = statusService.getById(id).get();
        return new ResponseEntity(status, HttpStatus.OK);
    }
    
    /*@PostMapping("/create")    
    public ResponseEntity<?> create(@RequestBody StatusDto statusDto){
        if(StringUtils.isBlank(statusDto.getName().get()))
            return new ResponseEntity(new Message("Name is required"), HttpStatus.BAD_REQUEST);
        Status status = new Status(statusDto.getName());
        statusService.save(status);
        return new ResponseEntity(new Message("Status created successfuly"), HttpStatus.OK);
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id") int id,@RequestBody StatusDto statusDto){
        if(!statusService.existsById(id))
            return new ResponseEntity(new Message("Task does not exist"), HttpStatus.NOT_FOUND);
        if(statusService.existsByName(statusDto.getName()) && statusService.getByName(statusDto.getName()).get().getId()!= id)
            return new ResponseEntity(new Message("Task name has already taken"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(statusDto.getName()))
            return new ResponseEntity(new Message("Name is required"), HttpStatus.BAD_REQUEST);
        Status status = statusService.getById(id).get();
        status.setName(statusDto.getName());
        statusService.save(status);
        return new ResponseEntity(new Message("Status updated successfuly"), HttpStatus.OK);
    }*/
    
    @ApiOperation("Method for delete status")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") int id){
        if(!statusService.existsById(id))
            return new ResponseEntity(new Message("Status does not exist"), HttpStatus.NOT_FOUND);
        statusService.delete(id);
        return new ResponseEntity(new Message("Status deleted successfuly"), HttpStatus.OK);
    }
}
