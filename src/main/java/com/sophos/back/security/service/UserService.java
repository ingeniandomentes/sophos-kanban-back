/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.security.service;

import com.sophos.back.security.entity.User;
import com.sophos.back.security.repository.UserRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ricardo
 */
@Service
@Transactional
public class UserService {
    
    @Autowired
    UserRepository userRepository;
    
    public List<User> list(){
        return userRepository.findAll();
    }
    
    public Optional<User> getById(int id){
        return userRepository.findById(id);
    }
    
    public Optional<User> getByUsername(String username){
        return userRepository.findByUsername(username);
    }
    
    public boolean existsById(int id){
        return userRepository.existsById(id);
    }
    
    public boolean existsByUsername(String username){
        return userRepository.existsByUsername(username);
    }
    
    public boolean existsByEmail(String email){
        return userRepository.existsByEmail(email);
    }
    
    public void save(User user){
        userRepository.save(user);
    }
    
    public void delete(int id){
        userRepository.deleteById(id);
    }
}
