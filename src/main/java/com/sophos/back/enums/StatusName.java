/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sophos.back.enums;

/**
 *
 * @author Ricardo
 */
public enum StatusName {
    BACK_LOG, TO_DO, IN_PROGRESS, REVIEW, DONE, REJECTED
}
